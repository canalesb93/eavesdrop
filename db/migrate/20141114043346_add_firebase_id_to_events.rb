class AddFirebaseIdToEvents < ActiveRecord::Migration
  def change
    add_column :events, :firebase_id, :string
  end
end
