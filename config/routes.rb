Rails.application.routes.draw do
  resources :users

  root to: 'visitors#index'

  get '/auth/:provider/callback' => 'sessions#create'
  get '/signin' => 'sessions#new', :as => :signin
  get '/signout' => 'sessions#destroy', :as => :signout
  get '/auth/failure' => 'sessions#failure'

  get '/events' => 'events#index', as: :events
  get '/events/:firebase_id' => 'events#show', as: :event
  post '/events/create' => 'events#create', as: :create_event

  post '/comments/create' => 'comments#create'

  post '/pictures/upload' => 'pictures#upload', as: :upload_picture
  get '/pictures/downloadd/:name' => 'pictures#download', as: :download_picture
end
