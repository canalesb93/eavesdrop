class SessionsController < ApplicationController

  def new
    if session[:user_id]
      redirect_to events_path
    else
      redirect_to '/auth/facebook'
    end
  end

  def create
    if session[:user_id]
      redirect_to events_path
    else
      auth = request.env["omniauth.auth"]
      user = User.where(:provider => auth['provider'],
                        :uid => auth['uid'].to_s).first || User.create_with_omniauth(auth)
      reset_session
      session[:user_id] = user.id
      redirect_to events_path, :notice => 'Signed in!'
    end
  end

  def destroy
    reset_session
    redirect_to root_url, :notice => 'Signed out!'
  end

  def failure
    redirect_to root_url, :alert => "Authentication error: #{params[:message].humanize}"
  end

end
