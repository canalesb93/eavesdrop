class VisitorsController < ApplicationController
  def index
    redirect_to events_path if session[:user_id]
  end
end
