class PicturesController < ApplicationController
  def upload
    uploaded_file = params[:picture]
    new_path, name = tmp_path
    FileUtils.mv uploaded_file.path, new_path

    render json: { image: download_picture_path(name) }
  end

  def download
    disposition = params[:disposition]
    disposition ||= 'attachment'
    name = params[:name]
    send_file "#{Rails.root}/uploaded_pictures/" + name, filename: name,
      disposition: disposition
  end

  def tmp_path
    loop do
      name = (1..12).map { '0123456789abcdefghijklmnopqrstuvwxyz'.chars.sample }.join
      tmp_path = Rails.root.join('uploaded_pictures', name)
      return [tmp_path.to_s, name] unless File.exist? tmp_path
    end
  end
end
