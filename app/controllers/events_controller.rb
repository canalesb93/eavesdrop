class EventsController < ApplicationController

  def index
    @events = Event.all
  end

  def show
    @event = Event.find_by_firebase_id(params[:firebase_id])
  end

  def create
    event = Event.new(permitted_params[:event])
    base_uri = 'https://eavesdrop.firebaseio.com/'
    firebase = Firebase::Client.new(base_uri)
    response = firebase.push("events", { name: event.name,
                                         latitude: event.latitude || 0,
                                         longitude: event.longitude|| 0 })
    event.firebase_id = response.body["name"]
    event.save
    firebase.push("events/#{event.firebase_id}/users", current_user.uid)
    redirect_to event_path(event.firebase_id)
  end

  private

  def permitted_params
    params.permit(event: [:name, :latitude, :longitude, :firebase])
  end
end
